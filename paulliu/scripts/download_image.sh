#!/bin/bash

IMAGE_URL=http://localhost/~paulliu/rpb-console-image-imx7s-warp-20171215161415-36.rootfs.sdcard.gz

mkdir -p /tmp/warp7-test
pushd /tmp/warp7-test
rm -f rpb-console-image-*.sdcard.*
wget -c $IMAGE_URL
popd
