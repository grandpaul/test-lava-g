#!/bin/sh

# download the image
echo Download image
./download_image.sh

# boot to u-boot
echo Boot to U-Boot
./boot_to_u-boot.exp
sleep 5

# enter ums mode
echo UMS mode start
./run_ums.exp
sleep 15

# install the image
echo start writing image
gzip -d -c /tmp/warp7-test/rpb-console-image-imx7s-warp-20171215161415-36.rootfs.sdcard.gz | nc -N localhost 29543
sleep 20

# stop ums mode
echo UMS mode stop
./stop_ums.sh

# boot to linux
./boot_to_linux.exp
